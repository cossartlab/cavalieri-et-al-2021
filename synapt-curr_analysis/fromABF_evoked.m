% all ABF
fp = '\\netdata\EqpCossart\Davide Cavalieri\Experiments\PATCH CLAMP\VC\__RAW_FILES\2017_12_20\';
fn = '17d20002.abf';
f=abfload(strcat(fp,fn));

F2 = f(:,1);%/10;
figure, plot(F2)

%% single traces
fn = '2017_07_19_03_IPSC_PP.abf';
f=abfload(fn);

f2 = f(:,1)*10/gain;
figure, plot(f2)

%%
%%
%Store information
prompt1={'Enter file name (under 2018_... form)  : ','Enter event type (ex. eEPSC)  : ', 'What is the polarity of the events? (+ or -)  : '};
title1 = 'Load trace'; dims1=[1,50];
definput1 = {'2019_','',''}; 
answer1 = inputdlg(prompt1,title1,dims1,definput1);


fileinfo.TraceName = answer1{1};
fileinfo.Protocol = answer1{2};
polarity = answer1{3};


nSweeps=10;
lengthSweep = 11200;
%gain = 5;

sr = 20000;
baseline_points = 1000; %for Rs calculation
Step= 0.01; %Volts 

onset = 5; %percentage of current to calculate onset


%% 
findpeaks(-f2,'MinPeakProminence',400,'Npeaks',10,'MinPeakDistance',100000);

[~,locs]= findpeaks(-f2,'MinPeakProminence',400,'Npeaks',10,'MinPeakDistance',90000);
%%
evokSweep = zeros(lengthSweep,nSweeps);
for i=1:nSweeps
    if polarity=='+'
    evokSweep(:,i)=f2(locs(i)-500:locs(i)+lengthSweep-501);
    else
    evokSweep(:,i)=-f2(locs(i)-500:locs(i)+lengthSweep-501);
    end
    figure, plot(evokSweep(:,i))
end


mean_trace = mean(evokSweep,2);
mean_trace= mean_trace-median(mean_trace(1:baseline_points)); %mean evoked trace and zeroed
figure, plot(mean_trace)

stim = zeros(length(mean_trace),2);
stim(:,1) = (0:1/sr:length(mean_trace)/sr-1/sr)';

%%
% check if no trace saturates
Satur = zeros(1,nSweeps);
for i=1:nSweeps,
    if length(find(evokSweep(:,i) == max(evokSweep(:,i))))>2 & sum(diff(find(evokSweep(:,i) == max(evokSweep(:,i))))==1)>5;
    Satur(i) =1;
    disp (strcat('SWEEP #',num2str(i),' saturates - DISCARDED'))
    end
end

nSweeps = length(Satur(Satur==0)); %number of sweeps without saturation
evokSweep = evokSweep(:,find(Satur==0)); %new matrix with non_saturating traces

%find baseline
evokSweep_baseline = zeros(1,nSweeps);
for i=1:nSweeps,
    evokSweep_baseline(i) = mean(evokSweep(1:baseline_points,i));
end


%find Rs
Rs = zeros(nSweeps,1);
if polarity == '+'
    for i=1:nSweeps
    [curr, time] = findpeaks(-evokSweep(1:1800,i)+evokSweep_baseline(i),'MinPeakHeight',200,'NPeaks',1);
    Rs(i)=(Step/curr*10^12)/10^6; %values in MegaOhms
    end
elseif polarity == '-'
    for i=1:nSweeps
    [curr, time] = findpeaks(evokSweep(1:1800,i)-evokSweep_baseline(i),'MinPeakHeight',200,'NPeaks',1);
    Rs(i)=(Step/curr*10^12)/10^6; %values in MegaOhms
    end
end   

mean_Rs = mean(Rs);
RsDisplay = ['mean Rs = ', num2str(mean_Rs),' MOhms']; disp (RsDisplay);

%Checks values of Rs
if sum(Rs > 30)~=0;
    disp ('*********       Series Resistance TOO HIGH            *********')
    exc = find(Rs > 30);
    disp ('Steps exceeding 30 MOhms:')
    disp (num2str(exc))
    disp ('DISCARDED')
    
    nSweeps = nSweeps- length(exc); %number of sweeps without saturation
    evokSweep(:,exc) = []; %new matrix with non_saturating traces
end



%Checks values of Rs to be constant
if sum(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))~=0,
    disp ('*********       Series Resistance VARIES MORE than 20%            *********')
    exc = find(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))+3;
    disp ('Steps varying more than 20%:')
    disp (num2str(exc))
    disp ('DISCARDED')
    nSweeps = nSweeps- length(exc); %number of sweeps that pass 20% thresh
    evokSweep(:,exc) = []; %new matrix with ok Rs traces
end


%%
%Store information
fileinfo.SamplingRate = sr;
fileinfo.Traces = evokSweep;
fileinfo.StimTrace = stim(:,2);


%Calculate maximum 
figure, plot(mean_trace);
DoublePeak = inputdlg('Is there a double peak? (y/n) = ','');
if DoublePeak{1} == 'n';
    %[amp0, temp0] = findpeaks(mean_trace(Tstim(1):Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    [amp0, tempo] = findpeaks(mean_trace(Tstim(1)+10:Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    temp0 = Tstim(1)+tempo+9; %time index on whole trace
else 
    temp = mean_trace(Tstim(1)+50:Tstim(1)+500); %segment containing peaks
    lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    amp0 = temp(find(lm~=0,1)); %time point of local maxima
    temp0 =  Tstim(1)+49+find(lm~=0,1); %time index on whole trace
end

bl0 = mean(mean_trace(Tstim(1)-50:Tstim(1))); %baseline1
peak0 = amp0 - bl0; %amplitude figur = peak - baseline

Rise = mean_trace(Tstim+5:temp0); %ramping phase of event
Rise_tv = (0:1/sr:length(Rise)/sr-1/sr)'; %time vector of rise time

%Calculate onset and delay
Ionset = (peak0/100)*onset; %value of current at onset, in pA
ind_closest = dsearchn(Rise,Ionset); %closest point to I onset
delay = ind_closest*1000/sr; %delay in ms

%
delayDisplay = ['Delay to onset = ', num2str(delay), ' ms']; disp (delayDisplay);

%Calculate Rise Time and Tau
Rise10 = (peak0/100)*10; %value of current at 10% peak
Rise90 = (peak0/100)*90; %value of current at 90% peak
ind_Rise10 = dsearchn(Rise,Rise10); %closest point to 10% peak
ind_Rise90 = dsearchn(Rise,Rise90); %closest point to 90% peak
RiseTime = (ind_Rise90-ind_Rise10)*1000/sr; %Rise Time in ms

if DoublePeak{1} == 'n';
        if lengthSweep > 10000,
            decay = mean_trace(temp0:temp0+4000);%in sec, DELAY from stim peak and evoked peak
        else
            decay = mean_trace(temp0:temp0+2350);%in sec, DELAY from stim peak and evoked peak
        end
elseif DoublePeak{1} == 'y',
    temp2 =  find(temp==max(temp)); %time index on whole trace
    if lengthSweep >10000,
    decay = mean_trace(Tstim(1)+50+temp2:Tstim(1)+50+temp2+4000);%in sec, DELAY from stim peak and evoked peak
    else
    decay = mean_trace(Tstim(1)+50+temp2:Tstim(1)+50+temp2+2350);%in sec, DELAY from stim peak and evoked peak    
    end
    peak0 = max(temp);
end

decay_tv = (0:1/sr:length(decay)/sr-1/sr)'; %time vector of decay
decay90 = dsearchn(decay,peak0/100*90);
decay10 = dsearchn(decay,peak0/100*10);
decay37 = dsearchn(decay,peak0/100*37);
DecayTime = abs(decay37-decay90)*1000/sr;
HalfWidth = (abs(length(Rise)- dsearchn(Rise,peak0/100*50)) +abs(dsearchn(decay,peak0/100*50))) *1000/sr;    %in ms
Area = trapz([Rise_tv(ind_Rise10:end) ;Rise_tv(end)+decay_tv(1:decay10)], [Rise(ind_Rise10:end); decay(1:decay10)])*1000; %in pA*ms

close all


ans = 'n';
bounds = {'90','10'};
while ans == 'n'
    decay_bottom = dsearchn(decay,peak0/100*str2double(bounds{2}));
    decay_top = dsearchn(decay,peak0/100*str2double(bounds{1}));
    f = ezfit(decay_tv(decay_top:decay_bottom), decay(decay_top:decay_bottom),'exp'); %exponential fit
    tau = -1/f.m(2)*1000;  %time costant of decay
    figure, plot(decay_tv,decay,'k'), showfit(f, 'dispeqboxmode','off');
    xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')             

    ans_cell = inputdlg('Are you satisfied with the fit? (y/n)','');
    ans = ans_cell{1};
    if ans =='n';
        bounds = inputdlg({'Upper boundary of fit (75 to 99)?','Lower boundary of fit (1 to 20)?'},'',[1 35],bounds);
    end
    close
end

%%
%PLOT & STORE
figure,
subplot(211), plot(stim(:,1)*1000, evokSweep), title('Overlay')
subplot(223),plot(stim(:,1)*1000,stim(:,2)*10,'r')
            hold on, plot(stim(:,1)*1000,mean_trace,'k'), xlim([90 200])
            hold on, plot((temp0-1)*1000/sr,amp0,'b*')
            hold on, plot(xlim, [Ionset Ionset],'g--');
            hold on, plot(1000*(ind_closest+Tstim+4)/sr, mean_trace(ind_closest+Tstim+4),'bo');
            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')

subplot(224),plot(decay_tv,decay,'k'), showfit(f, 'dispeqboxmode','off');
            hold on, plot(decay_tv(decay10),(peak0/100*10),'ro')
            hold on, plot(decay_tv(decay90),(peak0/100*90),'ro')
            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')
           
%
%Print Results 
PeakDisp = ['Peak Amplitude = ', num2str(peak0), ' pA']; disp (PeakDisp);
AreaDisp = ['Area = ', num2str(Area), ' pA*ms']; disp (AreaDisp);

RiseTimeDisplay = ['Rise time 10-90% = ', num2str(RiseTime), ' ms']; disp (RiseTimeDisplay);
DecayTimeDisplay = ['Decay time 90-37% = ', num2str(DecayTime), ' ms']; disp (DecayTimeDisplay);
HwDisplay = ['Half-width = ', num2str(HalfWidth),' ms']; disp (HwDisplay);
tauDisplay = ['Decay time constant (Tau) = ', num2str(tau)]; disp (tauDisplay);

%store data
fileinfo.Rs = mean_Rs;
fileinfo.Delay = delay;
fileinfo.Peak = peak0;
fileinfo.Area = Area;
fileinfo.RiseTime = RiseTime;
fileinfo.DecayTime = DecayTime;
fileinfo.HalfWidth = HalfWidth;
fileinfo.tau = tau;

%save file
temp_name = strcat(fileinfo.TraceName,'-',fileinfo.Protocol);
save(strcat(temp_name,'_fileinfo'), 'fileinfo')
savefig(strcat(temp_name,'_fig'))